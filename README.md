# Rick & Morty App.

## Descripción

* Es una aplicación móvil desarrollada en la tecnología de React Native, con la finalidad de consumir la API publica de Rick & Morty, en la cual se mostrara información referente a la famosa serie.

## Contenido
  
- [Descripción](#descripción)
- [Requisitos](#requisitos)
- [Dependencias con servicios y/o proyectos](#dependencias-con-servicios-y-proyectos)
- [Inicio de aplicación](#inicio-de-aplicacion)
- [Autor](#autor)
  
## Requisitos

* React Native 0.63.2
* NodeJS 10.17.0
* Visual Code 1.41.1
* Android Studio 3.5.3
* Xcode `(para IOS)` 
* CocaPods `(para IOS)`

## Dependencias con servicios y proyectos 

* Esta aplicación se alimenta de los datos proporcionados en la API de Rick & Morty, el sitio web con la documentación de la API es la siguiente: [Rick & Morty API](https://rickandmortyapi.com/)

## Inicio de aplicación 

### 1. Instalación de node_modules

Se debe de contar con node js (10.17.0 o superior, para revisar la versión instalada ejecutar)
```
node -v
```
para instalar los node modules ejecutar el comando:

```
 npm install
 
 yarn install
```
para linkear las dependencias necesarias ejecuar el comando:

```
 react-native link
```

(para ios)
```
cd ios

pod install
```

### 2. Correr proyecto
- **Android** : Dentro de la carpeta raiz, ejecutar el siguiente comando:
```
react-native run-android
```
- **IOS** : Dentro de la carpeta ios, abrir el archivo con extension .xcworkspace y correr el proyecto mediante Xcode



## Autor 
* Brandon Hernandez Torres
