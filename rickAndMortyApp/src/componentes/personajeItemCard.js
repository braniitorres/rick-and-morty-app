import React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from '../estilos/estilosPersonajeItemCard';

const PersonajeItemCard = ({item}) => {
  return (
    <View style={styles.card}>

        <View style={styles.containerImagen}>
            <Image
            style={ styles.image }
            source={{ uri: item.image }}
            />
        </View>

        <View style={styles.containerInformacion}>

            <View style={styles.containerHeaderCard} >
                <View style={styles.containerNombre}>
                    <Text style={styles.textoNombre}> {item.name} </Text>
                </View>
                <View style={styles.containerId}>
                    <Text style={styles.textoId} > #{item.id} </Text>
                </View>
            </View>

            <View style={styles.containerDescripcion} >
                <View style={styles.containerEspecie}>
                    <View style={styles.conatinerStatus}>
                        <Icon 
                        name={ item.status === 'Alive' ? 'heart' : item.status === 'Dead' ? 'skull-crossbones' : 'question-circle' } 
                        color={ item.status === 'Alive' ? 'green' : item.status === 'Dead' ? 'red' : 'gray' }
                        size={18}
                        />
                    </View>
                    <View style={styles.containerTipo}>
                        <Text style={styles.textoDescripción}> {item.species} </Text>
                    </View>
                </View>
                <View style={styles.containerOrigin}>
                    <Text style={styles.textoDescripcion}> Planet Origin: </Text>
                    <Text style={styles.textoPlanetaOrigen}> {item.origin.name} </Text>

                </View>
            </View>

        </View>

    </View>
  );
};

export default PersonajeItemCard;