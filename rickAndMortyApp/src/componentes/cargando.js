import React from 'react';
import {  View } from 'react-native';
import LottieView from 'lottie-react-native';
import styles from '../estilos/estilosCargando';

const Cargandon = () => {

    return(
        <View style={styles.main}>
            <LottieView
            source={require('../recursos/animaciones/planet-loading.json')}
            autoPlay
            loop
            />
        </View>
    )
};

export default Cargandon;