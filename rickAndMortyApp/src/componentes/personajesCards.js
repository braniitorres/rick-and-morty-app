import React from 'react';
import { View, FlatList, TouchableOpacity } from 'react-native';

import PersonajesItemCard from '../componentes/personajeItemCard';

const PersonajesCards = ({ personajesLista }) => {

  return (
    <View>
      <FlatList
        data={personajesLista}
        renderItem={({ item }) => (
          <TouchableOpacity>
            <PersonajesItemCard item={item} />
          </TouchableOpacity>
        )}
        keyExtractor={item => item.image}
      />
    </View>
  )
};



export default PersonajesCards;