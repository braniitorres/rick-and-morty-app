import {StyleSheet} from 'react-native';
import Color from '../utils/constantesColores';
import { responsiveWidth as wp, responsiveHeight as hp, responsiveFontSize as fz } from 'react-native-responsive-dimensions';

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: Color.colorFondoApp,
    },
    containerBusqueda: {
        flex: 1,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: wp(5),
        paddingVertical: hp(1.5)
    },
    containerContenido: {
        flex: 6,
        backgroundColor: 'transparent'
    },
    estiloBuscar: {
        backgroundColor: Color.colorFondoApp,
        borderColor: Color.colorVerde,
        borderWidth: 1,
        borderRadius: 20
    },
    inputEstilo: {
        color: Color.colorBlanco
    }
});

export default styles;