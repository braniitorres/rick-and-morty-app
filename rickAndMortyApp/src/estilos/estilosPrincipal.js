import {StyleSheet} from 'react-native';
import Color from '../utils/constantesColores';
import { responsiveWidth as wp, responsiveHeight as hp, responsiveFontSize as fz } from 'react-native-responsive-dimensions';

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: Color.colorFondoApp
    },
    cards: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    containerPaginacion: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'row',
        paddingVertical: hp(1),
    },
    containerPrev: {
        flex: 1,
        backgroundColor: 'transparent',
        alignItems: 'flex-start'
    },
    containerBoxPaginacion: {
        flex: 2,
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center'
    },
    containerNext: {
        flex: 1,
        backgroundColor: 'transparent',
        alignItems: 'flex-end',
    },
    iconoNext: {
        paddingRight: wp(4)
    },
    iconoPrev: {
        paddingLeft: wp(4)
    },
    textoPaginacion: {
        fontSize: fz(2),
        color: Color.colorPlata,
        fontStyle: 'italic'
    }
});

export default styles;