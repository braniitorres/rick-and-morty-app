import {StyleSheet} from 'react-native';
import Color from '../utils/constantesColores';

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: Color.colorFondoApp
    }
});

export default styles;