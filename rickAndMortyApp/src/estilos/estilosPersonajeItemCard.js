import {StyleSheet} from 'react-native';
import { 
    responsiveWidth as wp,
    responsiveHeight as hp,
    responsiveFontSize as fz } from 'react-native-responsive-dimensions';
import Color from '../utils/constantesColores';

const styles = StyleSheet.create({
    card: {
        flex: 1,
      backgroundColor: '#70707070',
      borderColor: 'green',
      borderWidth: 3,
      borderRadius: 5,
      margin: 10,
      flexDirection: "row"
    },
    containerImagen: {
        flex: 1.2,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 4
    },
    image: {
        width: wp(36),
        height: hp(18),
        borderRadius: 7
    },
    containerInformacion: {
        flex: 2,
        backgroundColor: 'transparent'
    },
    containerHeaderCard: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'row'
    },
    containerNombre: {
        flex: 3.5,
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center'
    },
    containerId: {
        flex: 1,
        backgroundColor: 'transparent',
        alignItems: 'center',
        paddingTop: 8
    },
    textoNombre: {
        fontSize: fz(2.5),
        color: Color.colorBlanco,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    textoId: {
        fontSize: fz(2.2),
        color: Color.colorVerde,
        borderWidth: 1.5,
        borderRadius: 12,
        borderColor: Color.colorVerde,
        textAlign: 'center',
        paddingHorizontal: 2,
    },
    containerDescripcion: {
        flex: 3,
        backgroundColor: 'transparent'
    },
    containerEspecie: {
        flex: 1,
        flexDirection: 'row'
    },
    conatinerStatus: {
        flex: 1,
        backgroundColor: 'transparent',
        alignItems: 'center'
    },
    containerTipo: {
        flex: 4,
        backgroundColor: 'transparent',
    },
    textoDescripción: {
        fontSize: fz(2),
        color: Color.colorBlanco,
        textAlign: 'left',
    },
    containerOrigin: {
        flex: 2,
        backgroundColor: 'transparent',
        paddingLeft: 11
    },
    textoDescripcion: {
        fontSize: fz(1.5),
        color: Color.colorPlata,
    },
    textoPlanetaOrigen: {
        fontSize: fz(2),
        color: Color.colorBlanco
    }

  });

export default styles;