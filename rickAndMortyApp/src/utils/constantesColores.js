
//COLORES
const colorFondoApp = '#202329';
const colorVerde = '#97CE4C';
const colorGris = '#707070';
const colorBlanco = '#FFFFFF';
const colorTextoGris = '#413434';
const colorPlata = 'silver';

const colores = {
    colorFondoApp,
    colorVerde,
    colorGris,
    colorBlanco,
    colorTextoGris,
    colorPlata
};

module.exports = colores;