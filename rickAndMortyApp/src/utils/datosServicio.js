
//Datos Servicios
const endPointRnMApi = 'https://rickandmortyapi.com/api/';
const uriPersonajes = 'character/';
const uriPaginacion = '?page=';
const uriBusquedaPorNombre = '?name='

const servicios = {
    endPointRnMApi,
    uriPersonajes,
    uriPaginacion,
    uriBusquedaPorNombre
};

module.exports = servicios;