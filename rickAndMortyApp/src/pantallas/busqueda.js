import React, { useState } from 'react';
import { View } from 'react-native';
import Color from '../utils/constantesColores';
import { Searchbar } from 'react-native-paper';
//Componentes
import PersonajesCards from '../componentes/personajesCards';
import Cargando from '../componentes/cargando';
//Servicio
import { getBusquedaPorPersonaje } from '../servicios/rickAndMortyApiServicio';
//Estilos
import styles from '../estilos/estilosBusqueda';



const Busqueda = () => {

    const [busqueda, setBusqueda] = useState([]);
    const [personajesList, setPersonajesList] = useState([]);
    const [cargando, setCargando] = useState(false);


    const getPersonajeBuscado = async (nombre) => {
        setCargando(true);
        try {
            const datosObtenidos = await getBusquedaPorPersonaje(nombre);
            console.log('respuestaApi: ', datosObtenidos.results.length)
            setPersonajesList(datosObtenidos.results);
            setCargando(false);
        } catch (error) {
            console.log('Error: ', error.message);
            setCargando(false);
        }
    }

    return (
        <View style={styles.main} >

            <View style={styles.containerBusqueda}>
                <Searchbar
                    placeholder="Search your character"
                    value={busqueda}
                    onChangeText={text => { getPersonajeBuscado(text), setBusqueda(text) }}
                    style={styles.estiloBuscar}
                    iconColor={Color.colorVerde}
                    placeholderTextColor={Color.colorPlata}
                    inputStyle={styles.inputEstilo}
                />
            </View>
            <View style={styles.containerContenido}>
                {
                    cargando 
                    ?
                    <Cargando/>
                    :
                    <PersonajesCards
                    personajesLista={personajesList}
                />
                }
            </View>

        </View>
    )
}

export default Busqueda;