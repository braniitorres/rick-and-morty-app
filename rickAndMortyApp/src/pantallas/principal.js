import React, { useState, useEffect } from 'react'
import { View, StyleSheet, ScrollView, TouchableOpacity, Text } from 'react-native'
import Color from '../utils/constantesColores';
import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from '../estilos/estilosPrincipal';
//Componentes
import PersonajesCards from '../componentes/personajesCards';
import Cargando from '../componentes/cargando';
//Servicio
import { getPersonajes, getPorURL } from '../servicios/rickAndMortyApiServicio';


const Principal = () => {

    const [personajesList, setPersonajesList] = useState([]);
    const [informacionPaginas, setInformacionPaginas] = useState({});
    const [cargando, setCargando] = useState(false);
    const [paginaActual, setPaginaActual] = useState(1);

    useEffect(() => {
        getPersonajesRnM();
    }, []);


    //Obtener los personajes
    const getPersonajesRnM = async () => {
        setCargando(true);
        try {
            const datosObtenidos = await getPersonajes();
            console.log('respuestaApi: ', datosObtenidos.results.length)
            setPersonajesList(datosObtenidos.results);
            setInformacionPaginas(datosObtenidos.info);
            console.log('useState Info: ', informacionPaginas.pages);
            setCargando(false);
        } catch (error) {
            console.log('Error: ', error.message);
            setCargando(false);
        }
    };

    const cambioPaginacion = async (pagina, aumento) => {
        if (aumento) {
            setPaginaActual(paginaActual + 1);
        } else {
            setPaginaActual(paginaActual - 1);
        }

        setCargando(true);
        console.log('Pagina: ', pagina);
        setPersonajesList([]);
        setInformacionPaginas({});
        try {
            const datosObtenidos = await getPorURL(pagina);
            console.log('respuestaApi: ', datosObtenidos.results.length)
            setPersonajesList(datosObtenidos.results);
            setInformacionPaginas(datosObtenidos.info);
            console.log('useState Info: ', informacionPaginas.pages);
            setCargando(false);
        } catch (error) {
            console.log('Error: ', error.message);
            setCargando(false);
        }
    };


    return (
        cargando ? (
            <Cargando />
        )
            :
            (
                <View style={styles.main} >
                    <ScrollView style={styles.cards}>
                        <PersonajesCards
                            personajesLista={personajesList}
                        />
                        <View style={styles.containerPaginacion}>
                            <View style={styles.containerPrev}>
                                <TouchableOpacity onPress={() => cambioPaginacion(informacionPaginas.prev, false)} >
                                    <Icon name={'angle-left'} size={35} color={Color.colorVerde} style={styles.iconoPrev} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.containerBoxPaginacion}>
                                <Text style={styles.textoPaginacion}> Page: {paginaActual} - {informacionPaginas.pages} </Text>
                            </View>
                            <View style={styles.containerNext}>
                                <TouchableOpacity onPress={() => { cambioPaginacion(informacionPaginas.next, true) }} >
                                    <Icon name={'angle-right'} size={35} color={Color.colorVerde} style={styles.iconoNext} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            )
    )
}

export default Principal;