import { endPointRnMApi, uriPersonajes, uriPaginacion , uriBusquedaPorNombre} from '../utils/datosServicio';
import axios from 'axios';

export const getPersonajes = () => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url: `${endPointRnMApi}${uriPersonajes}`,
            headers: {
                'content-type': 'application/json',
              },
        })
        .then( (response) => {
            resolve(response.data);
        })
        .catch( function(error)  {
            console.log('Error: ', error.message);
            reject(error.message);
        })
    }); 
};

export const getPersonajesPorPagina = (pagina) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url: `${endPointRnMApi}${uriPersonajes}${uriPaginacion}${pagina}`,
            headers: {
                'content-type': 'application/json',
              },
        })
        .then( (response) => {
            resolve(response.data);
        })
        .catch( function(error)  {
            console.log('Error: ', error.message);
            reject(error.message);
        })
    }); 
}

export const getPorURL = (url) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url: `${url}`,
            headers: {
                'content-type': 'application/json',
              },
        })
        .then( (response) => {
            resolve(response.data);
        })
        .catch( function(error)  {
            console.log('Error: ', error.message);
            reject(error.message);
        })
    }); 
}

export const getBusquedaPorPersonaje = (nombre) => {
    console.log('Se esta buscando en: ', `${endPointRnMApi}${uriPersonajes}${uriBusquedaPorNombre}${nombre}`);
    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url: `${endPointRnMApi}${uriPersonajes}${uriBusquedaPorNombre}${nombre}`,
            headers: {
                'content-type': 'application/json',
              },
        })
        .then( (response) => {
            resolve(response.data);
        })
        .catch( function(error)  {
            console.log('Error en servicio: ', error.message);
            reject(error.message);
        })
    }); 
}