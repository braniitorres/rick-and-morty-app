import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Color from './src/utils/constantesColores';
//IMPORTAR VISTAS
import VistaBusqueda from './src/pantallas/busqueda';
import VistaPrincipal from './src/pantallas/principal';

//SE CREA EL TAB BAR
const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
      screenOptions={ ({ route }) => ({
        tabBarIcon: ({ color }) => {
          let iconName;
          let size;

          if (route.name === 'Principal') {
            iconName = 'pastafarianism';
            size = 30
          } else if ( route.name === 'Busqueda') {
            iconName = 'wpexplorer';
            size = 35
          }

          return <Icon name={iconName} size={size} color={color} />
        },
      })}
      tabBarOptions={{
        showLabel: false,
        tabStyle: ({ backgroundColor: Color.colorFondoApp, borderTopColor: Color.colorVerde, borderTopWidth: 1.5 }),
        activeTintColor: Color.colorVerde,
        inactiveTintColor: Color.colorGris
      }}
      >

        <Tab.Screen  name="Principal" component={ VistaPrincipal } />
        <Tab.Screen name="Busqueda" component={ VistaBusqueda } />

      </Tab.Navigator>
    </NavigationContainer>
  );
}